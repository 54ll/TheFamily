#TheFamily

## [We are 伐 木 累 ！](https://family.liuzy88.com/)
* 家族亲人信息管理
* 显示家族树
* 显示生日目录
* 公历农历转换
* 手机浏览一样棒

## 界面预览
- ![家族树](https://liuzy88.com/files/family1.png)
- ![生日墙](https://liuzy88.com/files/family2.png)
- ![信息列表](https://liuzy88.com/files/family3.png)
- ![添加界面](https://liuzy88.com/files/family4.png)
- ![查询亲人](https://liuzy88.com/files/family5.png)

## 联系我
* Name：Liuzy
* QQ：416657468
* WeiXin：secondsun
* Email：[13162165337@163.com](mailto:13162165337@163.com)

## 欢迎土豪点这里[打赏Liuzy](https://liuzy88.com/donate/)，我会更有动力的 !