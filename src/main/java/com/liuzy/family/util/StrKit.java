package com.liuzy.family.util;

import java.util.concurrent.ThreadLocalRandom;

public class StrKit {
	public static boolean isBlank(String str) {
		return str == null || str.length() == 0 || str.trim().length() == 0;
	}
	public static boolean notBlank(String str) {
		return str == null || "".equals(str.trim()) ? false : true;
	}
	public static Integer firstId(String ids) {
		return Integer.parseInt(ids.split(",")[0]);
	}
	public static String randomNum(int count) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			sb.append(ThreadLocalRandom.current().nextInt(10));
		}
		return sb.toString();
	}
}
