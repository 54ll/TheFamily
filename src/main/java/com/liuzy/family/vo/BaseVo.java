package com.liuzy.family.vo;

/**
 * 1.setData(obj)
 * 2.setCount(int)
 * 3.setTotalCount(int)
 *
 * @author liuzy
 * @version 2015年6月13日
 */
public class BaseVo {
    /**
     * 数据
     */
    private Object data;
    /**
     * 有几条
     */
    private int count;
    /**
     * 第几页
     */
    private int page = 1;
    /**
     * 每页几条
     */
    private int rows = 10;
    /**
     * 共有几条
     */
    private int totalCount;
    /**
     * 共有几页
     */
    private int totalPage;

    public BaseVo() {
    }
    
    public BaseVo(int page, int rows) {
    	this.page = page;
    	this.rows = rows;
    }

    public BaseVo(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        if (page <= 0) {
            this.page = 1;
        } else {
            this.page = page;
        }
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        if (rows <= 0) {
            this.rows = 10;
        } else {
            this.rows = rows;
        }
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        //计算共有几页totalPage,上一页是第几页upPage,下一页是第几页downPage
        if (totalCount > 0) {
            this.totalPage = totalCount <= rows ? 1 : (totalCount % rows == 0 ? totalCount / rows : totalCount / rows + 1);
        }
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

}
