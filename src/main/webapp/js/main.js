var myFamily, familyTree, birthdayWall;
require([ "myFamily", "familyTree", "birthdayWall" ], function() {
	var menuSelect = function(tag) {
		$('#welcome').parent("li").attr("class", "");
		$('#myFamily').parent("li").attr("class", "");
		$('#familyTree').parent("li").attr("class", "");
		$('#birthdayWall').parent("li").attr("class", "");
		$('#' + tag).parent("li").attr("class", "nav-current");
	}
	$('#welcome').click(function() {
		menuSelect("welcome");
		$('#main-content').load('welcome.html');
	});
	$('#myFamily').click(function() {
		menuSelect("myFamily");
		$('#main-content').load('myFamily.html', function() {
			myFamily = require("myFamily");
			myFamily.init();
		});
	});
	$('#familyTree').click(function() {
		menuSelect("familyTree");
		$('#main-content').load('familyTree.html', function() {
			familyTree = require("familyTree");
			familyTree.init();
		});
	});
	$('#birthdayWall').click(function() {
		menuSelect("birthdayWall");
		$('#main-content').load('birthdayWall.html', function() {
			birthdayWall = require("birthdayWall");
			birthdayWall.init();
		});
	});
	menuSelect("welcome");
	$('#main-content').load('welcome.html');
});
